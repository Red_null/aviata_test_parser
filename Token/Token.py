"""
    Loads and compose token for API connection
"""


class Token:
    def __init__(self):
        try:
            token_file = open('Token/access.token')
            self.token = token_file.readline()
        except FileNotFoundError:
            print('! Token file not found: access.token !')
            print('! Using default token !')
            self.token = ''  # place default token here

    def get_authorization_string(self):
        return 'Bearer {}'.format(self.token)
