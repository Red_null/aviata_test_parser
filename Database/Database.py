"""
    Database wrapper
"""
import sqlite3 as database


class TicketCacheDatabase:
    def __init__(self):
        self.connection = database.connect('Database/tickets_cache.sqlite')
        cursor = self.connection.cursor()
        cursor.execute(
            'create table if not exists tickets('
            'id TEXT constraint tickets_pk primary key,'
            'source text not null,'
            'destination text not null,'
            'price integer not null,'
            'date TEXT);'
        )
        self.connection.commit()

    def execute_one(self, query, value):
        cursor = self.connection.cursor()
        cursor.execute(query, value)
        self.connection.commit()

    def execute_many(self, query, value: list):
        cursor = self.connection.cursor()
        cursor.executemany(query, value)
        self.connection.commit()

    def truncate(self):
        cursor = self.connection.cursor()
        cursor.execute('delete from tickets;')
        self.connection.commit()

    def get_cache(self, limit=40):
        cursor = self.connection.cursor()
        if limit is None:
            query = 'select * from tickets;'
            cursor.execute(query)
        else:
            query = 'select * from tickets limit ?;'
            cursor.execute(query, (limit,))

        result = cursor.fetchall()
        self.connection.commit()
        return result

    def __del__(self):
        self.connection.close()
