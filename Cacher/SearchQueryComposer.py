"""
    Compose a query for API based on direction and departure date
"""
from datetime import datetime


class SearchQueryComposer:
    def __init__(self, direction: str,  # direction format: {FROM}-{TO}
                 departure_date: datetime):
        direction_divider = direction.index('-')
        self.source = direction[0:direction_divider].upper()
        self.destination = direction[direction_divider + 1:].upper()
        self.departure_date = departure_date
        self.optional_params = '1000E'

    def get_query_string(self):
        formatted_date = '{:%Y%m%d}'.format(self.departure_date)
        return '{}-{}{}{}'.format(self.source, self.destination,
                                  formatted_date, self.optional_params)
