"""
Simple logger
"""
import datetime


class LoadingLogger:
    log_file_name = 'log.txt'

    def __init__(self):
        log_file = open(self.log_file_name, 'a')
        log_file.close()

    def log(self, value: str):
        log_file = open(self.log_file_name, 'a')
        event_time = datetime.datetime.now()
        log_file.write('{}: {}'.format(event_time, value))
        log_file.close()

    def get_events(self, event_name: str):
        log_file = open(self.log_file_name, 'r')
        events = []
        for line in log_file.readlines():
            if event_name in line:
                events.append(event_name)
        return events

    def refresh(self):
        log_file = open(self.log_file_name, 'w')
        log_file.close()
