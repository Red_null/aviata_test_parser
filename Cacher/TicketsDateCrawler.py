"""
    Loads tickets info for the period of passed days_range using TicketsLoader
"""

from datetime import datetime
from datetime import timedelta

from Database.Database import TicketCacheDatabase
from Cacher.TicketsLoader import TicketsLoader
from Cacher.TicketsLoader import SearchQueryComposer


class TicketsDateCrawler:
    def __init__(self, direction, days_range=30):
        self.direction = direction
        self.days_range = days_range

    def save_data(self):
        db = TicketCacheDatabase()
        queries_pool = []

        for day in self._generate_dates():
            # print('{} at: {}'.format(self.direction, day.date()))
            res = self._process_date(self.direction, day)
            queries_pool.extend(res)

        db.execute_many('insert into tickets(id, source, destination, price, date) values (?,?,?,?,?);',
                        queries_pool)

    def _generate_dates(self):
        today = datetime.now()
        departure_dates = []

        step = timedelta(days=1)
        counter = self.days_range
        while counter > 0:
            today = today + step
            departure_dates.append(today)
            counter -= 1
        return departure_dates

    def _process_date(self, direction, day):
        api_query = SearchQueryComposer(direction, day)
        direction_tickets = TicketsLoader(api_query)
        tickets = direction_tickets.tickets_data.get('items')

        cheapest_tickets_indexes = []
        min_ticket_cost = None
        index = 0
        for ticket_info in tickets:
            ticket_cost = ticket_info.get('price').get('amount')
            if min_ticket_cost is None:
                min_ticket_cost = ticket_cost
                cheapest_tickets_indexes.append(index)
                index += 1
                continue
            if ticket_cost < min_ticket_cost:
                cheapest_tickets_indexes = [index]
            if ticket_cost == min_ticket_cost:
                cheapest_tickets_indexes.append(index)
            index += 1

        cheapest_tickets_queries = []  # it will stores cheapest tickets with the same amount, but different id's
        for index in cheapest_tickets_indexes:
            query_data = (
                tickets[index].get('id'),
                api_query.source, api_query.destination,
                tickets[index].get('price').get('amount'),
                str(day.date())
            )
            cheapest_tickets_queries.append(query_data)

        return cheapest_tickets_queries
