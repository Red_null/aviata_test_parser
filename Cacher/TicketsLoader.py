"""
    Loads ticket info based on composed query
"""
import requests

from Cacher.LoadingLogger import LoadingLogger
from Cacher.SearchQueryComposer import SearchQueryComposer
from Token.Token import Token


class TicketsLoader:
    api_url = 'https://api.platform.staging.aviata.team/airflow/search'

    request_header = {
        'Authorization': Token().get_authorization_string(),
        'Content-Type': 'application/json'
    }

    _attempts_limit = 3  # maximum number of requests

    def __init__(self, query: SearchQueryComposer):
        self.query = query.get_query_string()
        self.query_id = self.get_query_id()
        self.tickets_data: dict = self.load_tickets_data()

    def get_query_id(self):
        data = {'query': self.query}
        response = requests.post(self.api_url, json=data, headers=self.request_header)

        try:
            response.raise_for_status()
        except requests.exceptions.HTTPError as error:
            print(error)
            print(data)

        response_info: dict = response.json()
        return response_info.get('id')

    def load_tickets_data(self, attempt=1):
        url = '{}/results/{}'.format(self.api_url, self.query_id)

        def send_request():
            _response = requests.get(url, headers=self.request_header)
            _status = _response.json().get('status')
            return _response, _status

        response, status = send_request()
        while status != 'done':
            response, status = send_request()

        data = response.json()
        logger = LoadingLogger()

        # sometimes API returns an empty 'items' list; correct response after several attempts
        if len(data.get('items')) == 0:
            if attempt > self._attempts_limit:
                logger.log('Empty response: {}\n'.format(self.query))
                return response.json()

            self.query_id = self.get_query_id()  # update query id for next attempt

            return self.load_tickets_data(attempt + 1)
        logger.log('Success: {}\n'.format(self.query))
        return response.json()

    def __repr__(self):
        return '<{} - {}>'.format(self.query, self.query_id)
