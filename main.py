"""
    Cache stored in ./Database/tickets_cache.sqlite
    Logs generated in ./log.txt
    Created by Anton
"""

from datetime import datetime
from multiprocessing import Process
import schedule
import time

from Database.Database import TicketCacheDatabase
from Cacher.LoadingLogger import LoadingLogger
from Cacher.TicketsDateCrawler import TicketsDateCrawler

debug = True  # True - run cache update once; False every day at 00:00

cache_days_num = 30  # for testing
# cache_days_num = 30 # for production

directions = [
    'ALA-TSE',
    'TSE-ALA',
    'ALA-MOW',
    'MOW-ALA',
    'ALA-CIT',
    'CIT-ALA',
    'TSE-MOW',
    'MOW-TSE',
    'TSE-LED',
    'LED-TSE'
]


def progress_bar(current_status, total):
    progress_percent = int((current_status / total) * 10)
    progress_left = 10 - progress_percent
    print('\r[{}{}] {}/{}'.format('#' * progress_percent, '-' * progress_left, current_status,
                                  total), end='')


def update_cache():
    logger = LoadingLogger()
    database = TicketCacheDatabase()

    database.truncate()
    logger.refresh()

    start_time = datetime.now()

    ticket_crawlers = []
    for direction in directions:
        ticket_crawlers.append(TicketsDateCrawler(direction, days_range=cache_days_num))

    print('Loading tickets data from list:\n{} '.format(directions))
    print('Please wait...')
    processes = []
    for crawler in ticket_crawlers:
        crawling_process = Process(target=crawler.save_data, name=str(crawler.direction))
        processes.append(crawling_process)
        crawling_process.start()

    finished_processes = 0
    total_processes = len(processes)
    progress_bar(0, total_processes)
    for each in processes:
        each.join()
        finished_processes += 1
        progress_bar(finished_processes, total_processes)

    print('\nElapsing time: {}'.format(datetime.now() - start_time))

    empty_responses = logger.get_events('Empty response:')
    if len(empty_responses):
        print('Empty responses:')
        for each in empty_responses:
            print(each)
    else:
        print('Success')


def show_cache(print_limit=15):
    database = TicketCacheDatabase()
    cache = database.get_cache(limit=print_limit)  # limit=None for all cached tickets
    print('-------- Cached tickets --------')
    for each in cache:
        print('{data[0]}:{data[1]}-{data[2]} {data[3]} tg'.format(data=each))

    print('------ Cached tickets end ------')


if __name__ == '__main__':
    print('Script started')
    show_cache()
    print('Run {}'.format('once' if debug else 'every day at 00:00'))

    if debug:
        update_cache()
        show_cache(print_limit=50)
    else:
        update_cache()  # remove to not to run at script start
        schedule.every().day.at("00:00").do(update_cache)
        while True:
            schedule.run_pending()
            time.sleep(1)
